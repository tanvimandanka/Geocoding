package com.example.tanvi.geocoding

import android.location.Address
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import android.location.Geocoder
import android.widget.Toast
import com.google.android.gms.vision.barcode.Barcode
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import android.text.TextUtils




class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_geocoding.setOnClickListener(this)
        button_reverse_geocoding.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.button_geocoding -> {
                getAddressFromLocation(23.009522, 72.505869)
            }
            R.id.button_reverse_geocoding -> {
                geocodeAddress(text_address.text.toString(),Geocoder(this, Locale.ENGLISH))
            }
        }
    }


    /**
     * get Address From Location
     * @param latitude
     * @param longitude
     */
    private fun getAddressFromLocation(latitude: Double, longitude: Double) {

        val geocoder = Geocoder(this, Locale.ENGLISH)

        try {
            val addresses = geocoder.getFromLocation(latitude, longitude, 1)

            if (addresses.size > 0) {
                val fetchedAddress = addresses[0]
                val address = fetchedAddress.getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                Toast.makeText(this, address , Toast.LENGTH_SHORT).show()
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    /**
     * Method to get latitude,longitude from address.
     * @param addressStr-Address or zip code of city.
     * @param gc- Geocoder instance.
     */
    fun geocodeAddress(addressStr: String, gc: Geocoder) {
        var address: Address? = null
        var addressList: List<Address>? = null
        try {
            if (!TextUtils.isEmpty(addressStr)) {
                addressList = gc.getFromLocationName(addressStr, 5)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (null != addressList && addressList.size > 0) {
            address = addressList[0]
        }
        if (null != address && address.hasLatitude()
                && address.hasLongitude()) {
            Toast.makeText(this, "LatLong " + address.latitude.toString() + "," + address.longitude.toString(), Toast.LENGTH_SHORT).show()
        }
    }

}
